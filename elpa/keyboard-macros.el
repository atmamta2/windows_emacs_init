(fset 'triple-quotes
   [?\" ?\C-f ?\" ?\C-f ?\" ?\C-b backspace ?\C-b ?\C-b backspace ?\C-f])
(global-set-key (kbd "C-\"") 'triple-quotes)
(fset 'chain
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([1 11 99 108 97 115 115 32 25 40 99 104 97 105 110 101 114 46 67 104 97 105 110 5 58 return tab 100 101 102 32 95 95 105 110 105 116 95 95 40 115 101 108 102 6 58 return tab 115 117 112 101 114 46 95 95 105 110 105 116 95 95 40 25 44 32 115 101 108 102 6 return tab 119 105 116 104 32 115 101 108 102 46 105 110 105 116 95 115 99 111 112 101 40 6 58 return tab return tab tab tab 100 101 102 32 95 95 99 97 108 108 95 95 40 115 101 108 102 44 32 5 58 return tab] 0 "%d")) arg)))

